var util = require('util');


class CustomError {
    constructor(status, errorCode, errorMsg) {
        this.status = status;
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
        Error.captureStackTrace(this, this.constructor);
    }
}

util.inherits(CustomError, Error);

module.exports=CustomError;
