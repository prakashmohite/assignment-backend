const {createLogger,format,transports} = require("winston");
require("winston-daily-rotate-file");
const {printf}=format;
const config = require("config");


module.exports =  createLogger({
    format:format.combine(
        format.timestamp({
            format:"DD-MMM-YYYY T hh:mm:ss:SSS A"
        }),
        format.json()
        //customFileFormatter
    ),
    transports: [
        new (transports.DailyRotateFile)({
            filename: "%DATE%.PublicationLogger.log",
            dirname:config.logProperties.filePath,
            datePattern: "YYYY-MM-DD.",
            maxsize:config.logProperties.maxsize,
            level:config.logProperties.level,
            maxFiles:"30d"
        })
    ]
});