'use strict';
process.env['ORA_SDTZ'] = 'UTC';//setting the time zone to avoid the conversion time zone between database and app
process.env['UV_THREADPOOL_SIZE'] = 20;//by default 4 but need to set based on the number of hits need to be handled
//process.env['HTTPS_PROXY'] ='proxy.tcs.com:8080';
const express = require('express')
const bodyparser = require('body-parser')
const helmet = require('helmet');
const validate = require('express-validation');
const com = require('compression');
var cors = require('cors');
// var socket = require("socket.io");

const { transports } = require('winston');
const logger = require('./utilities').Logger;

const routers = require('./routes');

const app = express({ caseSensitive: true });//creating an express router
const { CustomError } = require('./utilities');
const version = require('./package.json').version

app.use(com());
app.disable('x-powered-by');//for security reasons
app.use(helmet.ieNoOpen());//for security reasons
app.use(helmet.xssFilter())//for security reasons
// app.use(bodyparser.json());//this helps in parsing the request body

app.use(bodyparser.json({ limit: '10mb' }));   // Increases the request limit to 5 MB
app.use(cors({
    exposedHeaders: 'token'
}));


app.get('/', function (req, res) {

    logger.info(" method / is called  ");
    res.send('Application is working, Version: ' + version)

});

app.use('/app/nasa', routers.nasa);

app.use(function (err, req, res, next) {
    //  res.status(400).json(err);

    // logger.info(' in error handler ' + JSON.stringify(err));
    // logger.info(' in error handler ' + err);
    if (err instanceof validate.ValidationError) {
        return res.status(err.status).json({
            "errorCode": 'VF01',
            "errorMessage": err.errors[0].messages[0].replace(new RegExp('\"', 'g'), ''),
            "retStatus": 'Fail'
        });
    }
    else if (err instanceof CustomError) {

        return res.status(err.status).json({
            "errorCode": err.errorCode,
            "errorMessage": err.errorMsg,
            "logTxtId": req.body.logTxtId,
            "retStatus": 'Fail'

        });
    }
    else {

        return res.status(500).json({
            "errorCode": 'ERR01',
            "errorMessage": 'Something went wrong.Please contact Support Team.',
            "retStatus": 'Fail'
        });
    }

});

process.env.PORT = 8090;

var server = app.listen(process.env.PORT, function () {
    if (process.env.NODE_ENV !== "stage" && process.env.NODE_ENV !== "production") {
        logger.add(new transports.Console({
            level: "debug"
        }));
    }
    logger.info("Server started for nasaApiIntegration......   in enviroment:" + process.env.NODE_ENV);
    logger.info("The server started listening..........." + process.env.PORT);
});

var exitHook = require('async-exit-hook');

// you can add async hooks by accepting a callback 
exitHook(callback => {
    logger.info("In exit Hook call back");
});

exitHook.uncaughtExceptionHandler(err => {
    logger.info("error" + err);
    logger.info("In uncaughtExceptionHandler:" + err);
});

exitHook.unhandledRejectionHandler(err => {
    logger.info("In unhandledRejectionHandler:" + err);
});

exitHook.uncaughtExceptionHandler((err, callback) => {
    logger.info("In uncaughtExceptionHandler:" + err);

});