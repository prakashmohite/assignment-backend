const { Logger, CustomError } = require("../utilities");
var request = require('request');
const config = require('config');

module.exports = (req) => {

    let logger = Logger.child({ fileName: `${__filename}` });
    logger.info(`nasaApi method START`);

    return new Promise((resolve, reject) => {

        try {

            logger.info(`request date : ${req.query.reqdate}`);           

            var option = {
                uri: config.nasaApi.endPoint + config.nasaApi.key + '&date=' + req.query.reqdate
            }

            logger.info(`nasaApi request option.uri : ${option.uri}`);

            request.get(option, function (error, response, body) {

                try {

                    if (error) {
                        logger.error(`nasaApi method ERROR NA01 ${error}`);
                        return reject(new CustomError(500, 'NA01-' + error.code, error.message));
                    }

                    if (response.statusCode == 200) {
                        logger.info(`nasaApi Success response : ${JSON.stringify(response.body)}`);
                        let resp =  JSON.parse(response.body);
                        logger.info(`nasaApi method END`);
                        return resolve(resp);
                    } else {
                        logger.error(`nasaApi method ERROR NA02 ${JSON.stringify(response.body)}`);
                        return reject(new CustomError(response.statusCode, 'NA02', 'Error received from API'));
                    }

                } catch (err) {
                    logger.error(`nasaApi method ERROR CATCHNA01 ${err}`);
                    return reject(new CustomError(500, 'CATCHNA01', 'Something went wrong. Please contact Support team.'));
                }

            })

        } catch (err) {
            logger.error(`nasaApi method ERROR CATCHNA02 ${err}`);
            return reject(new CustomError(500, 'CATCHNA02', 'Something went wrong. Please contact Support team.'));
        }

    })

}