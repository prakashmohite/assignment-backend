const { Logger, CustomError } = require("../utilities");
const nasaApi = require("../apiCalls/nasaApi");

module.exports = async (req, res, next) => {

    try {

        let logger = Logger.child({ fileName: `${__filename}` });

        logger.info(`fetchDtls STARTS`);

        let apiResp = await nasaApi(req);

        return res.status(200).json({
            'date': apiResp.date,
            'explanation': apiResp.explanation,
            'hdurl': apiResp.hdurl,
            'media_type': apiResp.media_type,
            'title': apiResp.title,
            'retStatus': 'Success',
            'errorCode': '',
            'errorMessage': ''
        });

    } catch (error) {
        next(error);
    }

}