# NODE ASSIGNMENT TEST

This is Backend services for calling nasa API's.

## NODE VERSION USED
v12.18.3

## TO START BACKEND SERVICE
"npm start"

## TO INSTALL NODE MODULES
"npm i"

## SERVICES CREATED

To get Image from NASA based on request date
get call : http://localhost:8090/app/nasa/fetchDtls?reqdate=2021-11-01