const router = require('express').Router({ caseSensitive: true });

const fetchDtls = require('../middleware/fetchDtls');

router.get('/fetchDtls', fetchDtls);

module.exports = router;


